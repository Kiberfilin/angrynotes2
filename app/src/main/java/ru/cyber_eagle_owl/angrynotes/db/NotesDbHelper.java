package ru.cyber_eagle_owl.angrynotes.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 20.01.2018.
 */

public class NotesDbHelper extends SQLiteOpenHelper {

    public NotesDbHelper(Context context) {
        super(context, NotesContract.DB_NAME, null, NotesContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (String query : NotesContract.CREATE_DATABASE_QUERIES) {
            sqLiteDatabase.execSQL(query);
        }
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            String query = String.format ("PRAGMA foreign_keys = %s","ON");
            db.execSQL(query);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion == 3) {
            sqLiteDatabase.execSQL(NotesContract.Images.CREATE_TABLE);
            sqLiteDatabase.execSQL(NotesContract.Files.CREATE_TABLE);
        }
        if (oldVersion == 2 && newVersion == 3) {
            sqLiteDatabase.execSQL(NotesContract.Files.CREATE_TABLE);
        }
    }
}
