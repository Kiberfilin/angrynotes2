package ru.cyber_eagle_owl.angrynotes;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;
import ru.cyber_eagle_owl.angrynotes.loaders.FileAddingLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.FileDeletingLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.ImageAddingLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.ImageDeletingLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.SavingNoteLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.UpdateNoteLoader;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteFilesAdapter;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteImagesAdapter;

/**
 * Created by User on 23.01.2018.
 */

public class CreateNoteActivity extends BaseNoteActivity {

    public static final String EXTRA_NOTE_ID = "note_id";
    private static final int REQUEST_CODE_PICK_FROM_GALLERY = 1;
    private static final int REQUEST_CODE_TAKE_PHOTO = 2;
    private static final int REQUEST_CODE_PICK_FILE = 3;

    private static final String VALUES_KEY = "contetnValues";
    private static final String NOTE_ID_KEY = "noteId";
    private static final String CURRENT_IMAGE_FILE_KEY = "currentImageFile";
    private static final String SAVED_IMAGES_FOR_NOTE_KEY = "savedImagesForNote";
    private static final String SAVED_FILES_FOR_NOTE_KEY = "savedFilesForNote";
    private static final String DELETING_IMAGE_URI_KEY = "deletingImageUri";
    private static final String DELETING_FILE_URI_KEY = "deletingFileUri";

    private static final int SAVING_NOTE_LOADER_ID = 2;
    private static final int UPDATE_NOTE_LOADER_ID = 3;
    private static final int IMAGE_ADDING_LOADER_ID = 4;
    private static final int IMAGE_DELETING_LOADER_ID = 5;
    private static final int FILE_ADDING_LOADER_ID = 6;
    private static final int FILE_DELETING_LOADER_ID = 7;

    private TextInputEditText titleEt;
    private TextInputEditText textEt;
    private TextInputLayout titleTil;
    private TextInputLayout textTil;

    private File currentImageFile;
    private ArrayList<String> savedImagesForNote;
    private ArrayList<String> savedFilesForNote;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(SAVED_IMAGES_FOR_NOTE_KEY, savedImagesForNote);
        outState.putStringArrayList(SAVED_FILES_FOR_NOTE_KEY, savedFilesForNote);
        outState.putSerializable(CURRENT_IMAGE_FILE_KEY, currentImageFile);
        super.onSaveInstanceState(outState);
    }

    private final NoteImagesAdapter.OnNoteImageLongClickListener onNoteImageLongClickListener =
            new NoteImagesAdapter.OnNoteImageLongClickListener() {
                @Override
                public void onImageLongClick(final long imageId) {
                    AlertDialog alertDialog = new AlertDialog.Builder(CreateNoteActivity.this)
                            .setMessage(R.string.message_delete_image)
                            .setPositiveButton(R.string.title_btn_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteImage(imageId);
                                }
                            })
                            .setNegativeButton(R.string.title_btn_no, null)
                            .create();

                    if (!isFinishing()) {
                        alertDialog.show();
                    }
                }
            };

    private final NoteFilesAdapter.OnNoteFileLongClickListener onNoteFileLongClickListener =
            new NoteFilesAdapter.OnNoteFileLongClickListener() {
                @Override
                public void onFileLongClick(final long fileId) {
                    AlertDialog alertDialog = new AlertDialog.Builder(CreateNoteActivity.this)
                            .setMessage(R.string.message_delete_file)
                            .setPositiveButton(R.string.title_btn_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteFile(fileId);
                                }
                            })
                            .setNegativeButton(R.string.title_btn_no, null)
                            .create();

                    if (!isFinishing()) {
                        alertDialog.show();
                    }
                }
            };

    private void deleteImage(long imageId) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(DELETING_IMAGE_URI_KEY, ContentUris.withAppendedId(NotesContract.Images.URI, imageId));
        getLoaderManager().initLoader(IMAGE_DELETING_LOADER_ID, bundle, new AsyncTaskLoaderCallback());
    }

    private void deleteFile(long fileId) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(DELETING_FILE_URI_KEY, ContentUris.withAppendedId(NotesContract.Files.URI, fileId));
        getLoaderManager().initLoader(FILE_DELETING_LOADER_ID, bundle, new AsyncTaskLoaderCallback());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        if (savedInstanceState != null) {
            currentImageFile = (File) savedInstanceState.getSerializable(CURRENT_IMAGE_FILE_KEY);
            savedImagesForNote = savedInstanceState.getStringArrayList(SAVED_IMAGES_FOR_NOTE_KEY);
            savedFilesForNote = savedInstanceState.getStringArrayList(SAVED_FILES_FOR_NOTE_KEY);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        titleEt = findViewById(R.id.title_et);
        textEt = findViewById(R.id.text_et);

        titleTil = findViewById(R.id.title_til);
        textTil = findViewById(R.id.text_til);

        noteId = getIntent().getLongExtra(EXTRA_NOTE_ID, -1);

        RecyclerView imagesRecyclerView = findViewById(R.id.images_rv);
        imagesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        noteImagesAdapter = new NoteImagesAdapter(null, onNoteImageLongClickListener);
        imagesRecyclerView.setAdapter(noteImagesAdapter);

        RecyclerView filesRecyclerView = findViewById(R.id.files_rv);
        filesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        noteFilesAdapter = new NoteFilesAdapter(null, onNoteFileLongClickListener);
        filesRecyclerView.setAdapter(noteFilesAdapter);

        if (noteId != -1) {
            initNoteLoader();
            initImagesLoader();
            initFilesLoader();
        }
    }

    protected void displayNote(Cursor cursor) {
        if (!cursor.moveToFirst()) {
            // Если не получилось перейти к первой строке — завершаем Activity
            finish();
        }

        String title = cursor.getString(cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_TITLE));
        String noteText = cursor.getString(cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_NOTE));

        titleEt.setText(title);
        textEt.setText(noteText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.create_note, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveNote();
                return true;

            case android.R.id.home:
                finish();
                return true;

            case R.id.action_attach:
                showImageSelectionDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveNote() {
        String title = titleEt.getText().toString().trim();

        String text = textEt.getText().toString().trim();

        boolean isCorrect = true;

        if (TextUtils.isEmpty(title)) {
            isCorrect = false;

            titleTil.setError(getString(R.string.error_empty_field));
            titleTil.setErrorEnabled(true);
        } else {
            titleTil.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(text)) {
            isCorrect = false;

            textTil.setError(getString(R.string.error_empty_field));
            textTil.setErrorEnabled(true);
        } else {
            textTil.setErrorEnabled(false);
        }
        Log.i("Test", "isCorrect = " + isCorrect);
        if (isCorrect) {
            long currentTime = System.currentTimeMillis();

            ContentValues contentValues = new ContentValues();

            contentValues.put(NotesContract.Notes.COLUMN_TITLE, title);
            contentValues.put(NotesContract.Notes.COLUMN_NOTE, text);

            if (noteId == -1) {
                contentValues.put(NotesContract.Notes.COLUMN_CREATED_TS, currentTime);
            }

            contentValues.put(NotesContract.Notes.COLUMN_UPDATED_TS, currentTime);

            Bundle bundle = new Bundle();
            bundle.putParcelable(VALUES_KEY, contentValues);

            if (noteId == -1) {
                //создаём новую заметку
                Log.i("Test", "Создаём новую заметку, для этого запускаем SAVING_NOTE_LOADER");
                getLoaderManager().initLoader(SAVING_NOTE_LOADER_ID, bundle, new AsyncTaskLoaderCallback());
            } else {
                //изменяем существующую заметку
                getLoaderManager().initLoader(UPDATE_NOTE_LOADER_ID, bundle, new AsyncTaskLoaderCallback());
            }
        }
    }

    private void showImageSelectionDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.title_dialog_attachment_variants)
                .setItems(R.array.attachment_variants, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                pickImageFromGallery();
                                break;
                            case 1:
                                takePhoto();
                                break;
                            case 2:
                                pickFile();
                                break;
                        }
                    }
                })
                .create();
        if (!isFinishing()) {
            alertDialog.show();
        }
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");

        startActivityForResult(intent, REQUEST_CODE_PICK_FROM_GALLERY);
    }

    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Создаём файл для изображения
        currentImageFile = createImageFile();

        if (currentImageFile != null) {
            // Если файл создался — получаем его URI
            Uri imageUri = FileProvider.getUriForFile(this,
                    "com.skillberg.notes.fileprovider",
                    currentImageFile);

            // Передаём URI в камеру
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

            startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO);
        }
    }

    private void pickFile() {
        showToast("Открывается диалог выбора файла", Toast.LENGTH_LONG);
        Intent intent = new Intent(this, FilePickerActivity.class);
        startActivityForResult(intent, REQUEST_CODE_PICK_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PICK_FROM_GALLERY:
                if (resultCode == RESULT_OK && data != null) {
                    // Получаем URI изображения
                    Uri imageUri = data.getData();
                    if (imageUri != null) {
                        try {
                            // Получаем InputStream, из которого будем декодировать Bitmap
                            InputStream inputStream = getContentResolver().openInputStream(imageUri);
                            // Копируем изображение в наш файл
                            File imageFile = createImageFile();
                            writeInputStreamToFile(inputStream, imageFile);
                            addImageToDatabase(imageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.e("Test", "Поймали IOException при попытке поработать с InputStream, " +
                                    "чтобы выбрать изображение из галлереи в " + getClass().getSimpleName());
                        }
                    }
                }
                break;

            case REQUEST_CODE_TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    // Сохраняем изображение
                    addImageToDatabase(currentImageFile);
                    // На всякий случай обнуляем файл
                    currentImageFile = null;
                }
                break;

            case REQUEST_CODE_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    String filePath = data.getStringExtra(FilePickerActivity.EXTRA_FILE_PATH);
                    Log.i("Test", "Filepath: " + filePath);
                    addFileToDatabase(filePath);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Nullable
    private File createImageFile() {
        // Генерируем имя файла
        String filename = System.currentTimeMillis() + ".jpg";

        // Получаем приватную директорию на карте памяти для хранения изображений
        // Выглядит она примерно так: /sdcard/Android/data/com.skillberg.notes/files/Pictures
        // Директория будет создана автоматически, если ещё не существует
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // Создаём файл
        File image = new File(storageDir, filename);
        try {
            if (image.createNewFile()) {
                return image;
            }
        } catch (IOException e) {
            Log.e("Error", "Поймали IOException при попытке image.createNewFile() в методе createImageFile() в файле CreateNoteActivity.java");
            e.printStackTrace();
        }

        return null;
    }

    private void writeInputStreamToFile(InputStream inputStream, File outFile) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(outFile);

        byte[] buffer = new byte[8192];
        int n;

        while ((n = inputStream.read(buffer)) > 0) {
            fileOutputStream.write(buffer, 0, n);
        }

        fileOutputStream.flush();
        fileOutputStream.close();
        inputStream.close();
    }

    private void addImageToDatabase(File file) {
        if (savedImagesForNote == null) {
            savedImagesForNote = new ArrayList<>();
        }
        savedImagesForNote.add(file.getAbsolutePath());

    }

    private void addFileToDatabase(String path) {
        if (savedFilesForNote == null) {
            savedFilesForNote = new ArrayList<>();
        }
        savedFilesForNote.add(path);
    }

    private void showToast(String text, int duration) {
        Toast.makeText(this, text, duration).show();
    }

    private class AsyncTaskLoaderCallback implements LoaderManager.LoaderCallbacks<Boolean> {
        @Override
        public Loader<Boolean> onCreateLoader(int i, Bundle bundle) {
            switch (i) {
                case SAVING_NOTE_LOADER_ID:
                    return new SavingNoteLoader(getApplicationContext(), (ContentValues) bundle.getParcelable(VALUES_KEY));
                case UPDATE_NOTE_LOADER_ID:
                    return new UpdateNoteLoader(getApplicationContext(), (ContentValues) bundle.getParcelable(VALUES_KEY), ContentUris.withAppendedId(NotesContract.Notes.URI, noteId));
                case IMAGE_ADDING_LOADER_ID:
                    return new ImageAddingLoader(getApplicationContext(), savedImagesForNote, bundle.getLong(NOTE_ID_KEY));
                case IMAGE_DELETING_LOADER_ID:
                    return new ImageDeletingLoader(getApplicationContext(), (Uri) bundle.getParcelable(DELETING_IMAGE_URI_KEY));
                case FILE_ADDING_LOADER_ID:
                    return new FileAddingLoader(getApplicationContext(), savedFilesForNote, bundle.getLong(NOTE_ID_KEY));
                case FILE_DELETING_LOADER_ID:
                    return new FileDeletingLoader(getApplicationContext(), (Uri) bundle.getParcelable(DELETING_FILE_URI_KEY));
                default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(Loader<Boolean> loader, Boolean result) {
            switch (loader.getId()) {
                case SAVING_NOTE_LOADER_ID:
                    Log.i("Test", "result = " + result);
                    if (result) {
                        if (savedImagesForNote != null || savedFilesForNote != null) {
                            String stringNoteId = SavingNoteLoader.createdNoteUri.getLastPathSegment();
                            Long tmpNoteId = Long.parseLong(stringNoteId);
                            Log.i("Test", "Из стринга распарсили лонг noteId: " + tmpNoteId);
                            Bundle bundle = new Bundle();
                            bundle.putLong(NOTE_ID_KEY, tmpNoteId);
                            if (savedImagesForNote != null) {
                                getLoaderManager().initLoader(IMAGE_ADDING_LOADER_ID, bundle, this);
                            }
                            if (savedFilesForNote != null) {
                                getLoaderManager().initLoader(FILE_ADDING_LOADER_ID, bundle, this);
                            }
                        }
                        showToast("Заметка сохранена", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Заметку не удалось сохранить", Toast.LENGTH_SHORT);
                    }
                    finish();
                    break;

                case UPDATE_NOTE_LOADER_ID:
                    if (result) {
                        if (savedImagesForNote != null || savedFilesForNote != null) {
                            Bundle bundle = new Bundle();
                            Log.i("Test", "В noteId сейчас значение: " + noteId);
                            bundle.putLong(NOTE_ID_KEY, noteId);
                            if (savedImagesForNote != null) {
                                getLoaderManager().initLoader(IMAGE_ADDING_LOADER_ID, bundle, this);
                            }
                            if (savedFilesForNote != null) {
                                getLoaderManager().initLoader(FILE_ADDING_LOADER_ID, bundle, this);
                            }
                        }
                        showToast("Заметка обновлена", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Заметку не удалось обновить", Toast.LENGTH_SHORT);
                    }
                    finish();
                    break;

                case IMAGE_ADDING_LOADER_ID:
                    if (result) {
                        showToast("Изображения прикреплены", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Прикрепить изображения не удалось", Toast.LENGTH_SHORT);
                    }
                    finish();
                    break;

                case IMAGE_DELETING_LOADER_ID:
                    if (result) {
                        showToast("Изображение удалено", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Удалить изображение не удалось", Toast.LENGTH_SHORT);
                    }
                    getLoaderManager().destroyLoader(IMAGE_DELETING_LOADER_ID);
                    break;

                case FILE_ADDING_LOADER_ID:
                    if (result) {
                        showToast("Файлы прикреплены", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Прикрепить файлы не удалось", Toast.LENGTH_SHORT);
                    }
                    finish();
                    break;

                case FILE_DELETING_LOADER_ID:
                    if (result) {
                        showToast("Файл удалён", Toast.LENGTH_SHORT);
                    } else {
                        showToast("Удалить файл не удалось", Toast.LENGTH_SHORT);
                    }
                    getLoaderManager().destroyLoader(IMAGE_DELETING_LOADER_ID);
                    break;
            }
        }

        @Override
        public void onLoaderReset(Loader<Boolean> loader) {

        }
    }
}