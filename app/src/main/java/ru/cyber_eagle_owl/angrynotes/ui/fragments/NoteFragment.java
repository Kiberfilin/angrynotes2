package ru.cyber_eagle_owl.angrynotes.ui.fragments;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.cyber_eagle_owl.angrynotes.CreateNoteActivity;
import ru.cyber_eagle_owl.angrynotes.R;
import ru.cyber_eagle_owl.angrynotes.db.NotesContract;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteFilesAdapter;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteImagesAdapter;

/**
 * Created by User on 09.03.2018.
 */

public class NoteFragment extends AbstractNoteFragment {

    private final static String NOTE_ID_KEY = "noteId";
    private TextView noteTv;
    private TextView noteHeader;

    public static NoteFragment newInstance(long noteId) {
        NoteFragment fragment = new NoteFragment();

        Bundle args = new Bundle();
        args.putLong(NOTE_ID_KEY, noteId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);

        noteHeader = view.findViewById(R.id.header_tv);

        noteTv = view.findViewById(R.id.text_tv);

        RecyclerView imagesRecyclerView = view.findViewById(R.id.images_rv);
        imagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        noteImagesAdapter = new NoteImagesAdapter(null, null);
        imagesRecyclerView.setAdapter(noteImagesAdapter);

        RecyclerView filesRecyclerView = view.findViewById(R.id.files_rv);
        filesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        noteFilesAdapter = new NoteFilesAdapter(null, null);
        filesRecyclerView.setAdapter(noteFilesAdapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();

        if (args != null && args.containsKey(NOTE_ID_KEY)) {
            noteId = args.getLong(NOTE_ID_KEY);
            if (noteId != -1) {
                initNoteLoader();
                initImagesLoader();
                initFilesLoader();

                view.findViewById(R.id.edit_fab).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editNote();
                    }
                });
            } else {
                getActivity().finish();
            }
            Log.i("Test", "URIonCreate = " + ContentUris.withAppendedId(NotesContract.Notes.URI, noteId));
        }
    }

    @Override
    protected void displayNote(Cursor cursor) {
        if (!cursor.moveToFirst()) {
            // Если не получилось перейти к первой строке — завершаем Activity
            getActivity().finish();
            return;
        }

        String title = cursor.getString(cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_TITLE));
        String noteText = cursor.getString(cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_NOTE));

        noteHeader.setText(title);
        noteTv.setText(noteText);
    }

    private void editNote() {
        Intent intent = new Intent(getActivity().getApplicationContext(), CreateNoteActivity.class);
        intent.putExtra(CreateNoteActivity.EXTRA_NOTE_ID, noteId);

        startActivity(intent);
    }
}

