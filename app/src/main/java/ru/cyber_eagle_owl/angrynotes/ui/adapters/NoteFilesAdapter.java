package ru.cyber_eagle_owl.angrynotes.ui.adapters;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.StringTokenizer;

import ru.cyber_eagle_owl.angrynotes.R;
import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Created by User on 09.02.2018.
 */

public class NoteFilesAdapter extends CursorRecyclerAdapter<NoteFilesAdapter.ViewHolder> {

    @Nullable
    private final OnNoteFileLongClickListener onNoteFileLongClickListener;

    public NoteFilesAdapter(Cursor cursor,
                            @Nullable OnNoteFileLongClickListener onNoteFileLongClickListener) {
        super(cursor);
        this.onNoteFileLongClickListener = onNoteFileLongClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.view_item_note_file, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        long fileId = cursor.getLong(cursor.getColumnIndexOrThrow(NotesContract.Files._ID));
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(NotesContract.Files.COLUMN_PATH));

        //Bitmap bitmap = BitmapFactory.decodeFile(filePath);

        //viewHolder.imageView.setImageBitmap(bitmap);
        StringTokenizer stringTokenizer = new StringTokenizer(filePath, "/");
        String token = null;
        while (stringTokenizer.hasMoreTokens()) {
            token = stringTokenizer.nextToken();
        }
        viewHolder.fileTextView.setText(token);
        //Log.i("Token", token);
        viewHolder.itemView.setTag(fileId);
    }

    /**
     * ViewHolder
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView fileImageView;
        private TextView fileTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            fileImageView = itemView.findViewById(R.id.fileImageView);
            fileTextView = itemView.findViewById(R.id.fileTextView);

            if (onNoteFileLongClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        long fileId = (long) ViewHolder.this.itemView.getTag();

                        onNoteFileLongClickListener.onFileLongClick(fileId);

                        return true;
                    }
                });
            }
        }

    }

    public interface OnNoteFileLongClickListener {
        void onFileLongClick(long fileId);
    }
}