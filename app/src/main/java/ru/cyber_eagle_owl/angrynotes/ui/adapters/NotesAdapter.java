package ru.cyber_eagle_owl.angrynotes.ui.adapters;

import android.database.Cursor;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;

import ru.cyber_eagle_owl.angrynotes.R;
import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Адаптер для заметок
 */
public class NotesAdapter extends CursorRecyclerAdapter<NotesAdapter.ViewHolder> implements Serializable {

    @Nullable
    private OnNoteClickListener onNoteClickListener;

    public NotesAdapter(Cursor cursor) {
        super(cursor);
    }

    public void setOnNoteClickListener(OnNoteClickListener onNoteClickListener) {
        this.onNoteClickListener = onNoteClickListener;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {

        int titleColumnIndex = cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_TITLE);
        String title = cursor.getString(titleColumnIndex);

        viewHolder.titleTv.setText(title);

        int dateColumnIndex = cursor.getColumnIndexOrThrow(NotesContract.Notes.COLUMN_UPDATED_TS);
        long updatedTs = cursor.getLong(dateColumnIndex);
        Date date = new Date(updatedTs);

        viewHolder.dateTv.setText(viewHolder.SDF.format(date));

        viewHolder.itemView.setTag(cursor.getLong(cursor.getColumnIndexOrThrow(NotesContract.Notes._ID)));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.view_item_note, parent, false);

        return new ViewHolder(view);
    }

    /**
     * View holder
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());

        private final TextView titleTv;
        private final TextView dateTv;

        public ViewHolder(View itemView) {
            super(itemView);

            this.titleTv = itemView.findViewById(R.id.title_tv);
            this.dateTv = itemView.findViewById(R.id.date_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Long noteId = (Long) view.getTag();
                    Log.i("Test", "***noteId = " + noteId);
                    if (onNoteClickListener != null) {
                        onNoteClickListener.onNoteClick(noteId);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Long noteId = (Long) view.getTag();
                    Log.i("Test", "***noteId = " + noteId);
                    if (onNoteClickListener != null) {
                        onNoteClickListener.onNoteLongClick(noteId, view);
                    }
                    return false;
                }
            });
        }
    }
    public interface OnNoteClickListener {
        void onNoteClick(long noteId);
        void onNoteLongClick(long noteId, View note);
    }
}