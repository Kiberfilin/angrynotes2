package ru.cyber_eagle_owl.angrynotes.ui.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import ru.cyber_eagle_owl.angrynotes.CreateNoteActivity;
import ru.cyber_eagle_owl.angrynotes.R;
import ru.cyber_eagle_owl.angrynotes.db.NotesContract;
import ru.cyber_eagle_owl.angrynotes.loaders.ImageDeletingLoader;
import ru.cyber_eagle_owl.angrynotes.loaders.NoteDeletingLoader;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NotesAdapter;

/**
 * Created by User on 25.02.2018.
 */

public class ListingFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private NotesAdapter notesAdapter= new NotesAdapter(null);

    private static final String DELETING_NOTE_URI_KEY = "deletingNoteUri";
    private static final int NOTES_LOADER_ID = 0;
    private static final int NOTE_DELETING_LOADER_ID = 1;

    AsyncTaskLoaderCallback callback = new AsyncTaskLoaderCallback();

    @Nullable
    private onNoteClickProcessingListener onNoteClickProcessingListener;

    @Nullable
    private onNoteDeletedListener onNoteDeletedListener;

    public static ListingFragment newInstance() {
        return new ListingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.notes_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(notesAdapter);

        getLoaderManager().initLoader(
                NOTES_LOADER_ID, // Идентификатор загрузчика
                null, // Аргументы
                this // Callback для событий загрузчика
        );

        view.findViewById(R.id.create_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateNoteActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i("Test", "ListingFragment = " + NotesContract.Notes.URI);
        return new CursorLoader(
                getActivity(), // контекст
                NotesContract.Notes.URI, // uri
                NotesContract.Notes.LIST_PROJECTION, // столбцы
                null, // Параметры выборки
                null, // аргументы выборки
                null // Сортировка по умолчанию
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.i("Test", "Load finished: " + cursor.getCount());

        cursor.setNotificationUri(getActivity().getContentResolver(), NotesContract.Notes.URI);
        notesAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    NotesAdapter.OnNoteClickListener onNoteClickListener = new NotesAdapter.OnNoteClickListener() {
        @Override
        public void onNoteClick(long noteId) {
            if (onNoteClickProcessingListener != null) {
                onNoteClickProcessingListener.onShortClick(noteId);
            }
        }

        @Override
        public void onNoteLongClick(final long noteId, View note) {
            PopupMenu popupMenu = new PopupMenu(getActivity(), note);
            popupMenu.inflate(R.menu.popup);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Intent intent;
                    switch (menuItem.getItemId()) {
                        case R.id.edit_note_popup:
                            intent = new Intent(getActivity(), CreateNoteActivity.class);
                            intent.putExtra(CreateNoteActivity.EXTRA_NOTE_ID, noteId);
                            startActivity(intent);
                            break;
                        case R.id.delete_note_popup:
                            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                    .setMessage(R.string.message_delete_note)
                                    .setPositiveButton(R.string.title_btn_yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            deleteNote(noteId);
                                        }
                                    })
                                    .setNegativeButton(R.string.title_btn_no, null)
                                    .create();

                            if (!getActivity().isFinishing()) {
                                alertDialog.show();
                            }

                            break;
                        case R.id.create_note_popup:
                            intent = new Intent(getActivity(), CreateNoteActivity.class);
                            startActivity(intent);
                            break;
                    }
                    return false;
                }
            });
            popupMenu.show();
        }
    };

    private void deleteNote(long noteId) {
        showToast("Запускаем лоадер для удаления заметки с Id = " + noteId, Toast.LENGTH_SHORT);
        Bundle bundle = new Bundle();
        bundle.putParcelable(DELETING_NOTE_URI_KEY, ContentUris.withAppendedId(NotesContract.Notes.URI, noteId));
        getLoaderManager().initLoader(NOTE_DELETING_LOADER_ID, bundle, callback);
    }

    private class AsyncTaskLoaderCallback implements LoaderManager.LoaderCallbacks<Boolean> {
        @Override
        public Loader<Boolean> onCreateLoader(int i, Bundle bundle) {
            switch (i) {
                case NOTE_DELETING_LOADER_ID:
                    return new NoteDeletingLoader(getActivity(), (Uri) bundle.getParcelable(DELETING_NOTE_URI_KEY));
                    default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(Loader<Boolean> loader, Boolean result) {
            switch (loader.getId()) {
                case NOTE_DELETING_LOADER_ID:
                    if (result) {
                        showToast("Заметка удалена", Toast.LENGTH_SHORT);
                        if (onNoteDeletedListener != null) {
                            onNoteDeletedListener.onNoteDeleted();
                        }
                    } else {
                        showToast("Удалить заметку не удалось", Toast.LENGTH_SHORT);
                    }
                    getLoaderManager().destroyLoader(NOTE_DELETING_LOADER_ID);
                    break;
            }
        }

        @Override
        public void onLoaderReset(Loader<Boolean> loader) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        notesAdapter.setOnNoteClickListener(onNoteClickListener);
        onNoteClickProcessingListener = (onNoteClickProcessingListener) context;
        onNoteDeletedListener = (onNoteDeletedListener) context;
    }

    @Override
    public void onDetach() {
        onNoteClickProcessingListener = null;
        onNoteDeletedListener = null;
        notesAdapter.setOnNoteClickListener(null);
        super.onDetach();
    }

    private void showToast(String text, int duration) {
        Toast.makeText(getActivity(), text, duration).show();
    }

    public interface onNoteClickProcessingListener {
        void onShortClick(long noteId);
    }

    public interface onNoteDeletedListener {
        void onNoteDeleted();
    }
}
