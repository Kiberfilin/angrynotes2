package ru.cyber_eagle_owl.angrynotes;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import ru.cyber_eagle_owl.angrynotes.ui.fragments.ListingFragment;
import ru.cyber_eagle_owl.angrynotes.ui.fragments.NoteFragment;

public class MainActivity extends AppCompatActivity implements ListingFragment.onNoteClickProcessingListener, ListingFragment.onNoteDeletedListener {

    private static String NOTE_FRAGMENT_TAG = "note";
    private static String LIST_FRAGMENT_TAG = "list";


    private boolean isTablet;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isTablet = getResources().getBoolean(R.bool.isTablet);

        fragmentManager = getFragmentManager();

        ListingFragment listingFragment = ListingFragment.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (fragmentManager.findFragmentByTag(NOTE_FRAGMENT_TAG ) == null && fragmentManager.findFragmentByTag(LIST_FRAGMENT_TAG) == null) {
            fragmentTransaction.add(R.id.fragment_container, listingFragment, LIST_FRAGMENT_TAG);
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }

    @Override
    public void onShortClick(long noteId) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (isTablet) {
            // у нас планшет
            if (fragmentManager.findFragmentByTag(NOTE_FRAGMENT_TAG) != null) {
                // Если фрагмент уже добавлен — заменяем
                fragmentTransaction.replace(R.id.note_fragment_container, NoteFragment.newInstance(noteId), NOTE_FRAGMENT_TAG);
                fragmentTransaction.addToBackStack(null);
            } else {
                // Иначе добавляем
                fragmentTransaction.add(R.id.note_fragment_container, NoteFragment.newInstance(noteId), NOTE_FRAGMENT_TAG);
            }
            fragmentTransaction.commit();
        } else {
            // у нас телефон
            Log.i("Test", "В методе onShortClick " + getClass().getSimpleName() +
                    " видим, что у нас телефон, а не планшет, поэтому заменяем фрагмент, который сейчас во фрагмент контейнере" +
                    " на NoteFragment: fragmentTransaction.replace(R.id.fragment_container, NoteFragment.newInstance(noteId), \"note\")");
            fragmentTransaction.replace(R.id.fragment_container, NoteFragment.newInstance(noteId), NOTE_FRAGMENT_TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onNoteDeleted() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (isTablet) {
            // у нас планшет
            Fragment tmpFragment = fragmentManager.findFragmentByTag(NOTE_FRAGMENT_TAG);
            if (tmpFragment != null) {
                // Если фрагмент уже добавлен очищаем контейнер
                fragmentTransaction.remove(tmpFragment);
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commitAllowingStateLoss();
        }
    }
}
