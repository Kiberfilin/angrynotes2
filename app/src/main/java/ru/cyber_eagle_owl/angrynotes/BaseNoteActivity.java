package ru.cyber_eagle_owl.angrynotes;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteFilesAdapter;
import ru.cyber_eagle_owl.angrynotes.ui.adapters.NoteImagesAdapter;

/**
 * Created by User on 10.02.2018.
 */
public abstract class BaseNoteActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    protected static final int LOADER_NOTE = 0;
    protected static final int LOADER_IMAGES = 1;
    protected static final int LOADER_FILES = 2;

    protected long noteId = -1;

    protected NoteImagesAdapter noteImagesAdapter;
    protected NoteFilesAdapter noteFilesAdapter;

    /**
     * Инициализируем загрузчик заметки
     */
    protected void initNoteLoader() {
        getLoaderManager().initLoader(
                LOADER_NOTE, // Идентификатор загрузчика
                null, // Аргументы
                this // Callback для событий загрузчика
        );
    }

    /**
     * Инициализируем загрузчик изображений
     */
    protected void initImagesLoader() {
        getLoaderManager().initLoader(
                LOADER_IMAGES,
                null,
                this
        );
    }
    /**
     * Инициализируем загрузчик файлов
     */
    protected void initFilesLoader() {
        getLoaderManager().initLoader(
                LOADER_FILES,
                null,
                this
        );
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_NOTE:
                return new CursorLoader(
                        this, // Контекст
                        ContentUris.withAppendedId(NotesContract.Notes.URI, noteId), // URI
                        NotesContract.Notes.SINGLE_PROJECTION, // Столбцы
                        null, // Параметры выборки
                        null, // Аргументы выборки
                        null // Сортировка по умолчанию
                );
            case LOADER_IMAGES:
                return new CursorLoader(
                        this,
                        NotesContract.Images.URI,
                        NotesContract.Images.PROJECTION,
                        NotesContract.Images.COLUMN_NOTE_ID + " = ?",
                        new String[]{String.valueOf(noteId)},
                        null
                );
            case LOADER_FILES:
                return new CursorLoader(
                        this,
                        NotesContract.Files.URI,
                        NotesContract.Files.PROJECTION,
                        NotesContract.Files.COLUMN_NOTE_ID + " = ?",
                        new String[]{String.valueOf(noteId)},
                        null
                );
            default:
                return null;
        }
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_NOTE:
                cursor.setNotificationUri(this.getContentResolver(), NotesContract.Notes.URI);
                displayNote(cursor);
                break;

            case LOADER_IMAGES:
                cursor.setNotificationUri(this.getContentResolver(), NotesContract.Images.URI);
                noteImagesAdapter.swapCursor(cursor);
                break;

            case LOADER_FILES:
                cursor.setNotificationUri(this.getContentResolver(), NotesContract.Files.URI);
                noteFilesAdapter.swapCursor(cursor);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Отображаем заметку. Этот метод должен быть реализован в Activity
     */
    protected abstract void displayNote(Cursor cursor);

}