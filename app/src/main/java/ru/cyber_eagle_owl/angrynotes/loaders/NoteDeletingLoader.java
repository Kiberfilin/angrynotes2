package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;

/**
 * Created by User on 11.02.2018.
 */

public class NoteDeletingLoader extends AsyncTaskLoader<Boolean> {
    private Uri uri;

    public NoteDeletingLoader(Context context, Uri uri) {
        super(context);
        this.uri = uri;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public Boolean loadInBackground() {
        return getContext().getContentResolver().delete(uri,
                null,
                null) != 0;
    }
}
