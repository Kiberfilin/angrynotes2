package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;

import android.net.Uri;

/**
 * Created by User on 01.02.2018.
 */

public class UpdateNoteLoader extends AsyncTaskLoader <Boolean> {
    private ContentValues contentValues;
    private Uri uri;

    public UpdateNoteLoader(Context context, ContentValues contentValues, Uri uri) {
        super(context);
        this.contentValues = contentValues;
        this.uri = uri;
    }

    @Override
    public Boolean loadInBackground() {
        return getContext().getContentResolver().update(uri, contentValues, null, null) != 0;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
