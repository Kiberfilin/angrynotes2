package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Created by User on 09.02.2018.
 */

public class FileAddingLoader extends AsyncTaskLoader<Boolean> {
    private ArrayList<String> savedFilesForNote;
    private Long noteId;
    public FileAddingLoader(Context context, ArrayList<String> savedFilesForNote, Long noteId) {
        super(context);
        this.savedFilesForNote = savedFilesForNote;
        this.noteId = noteId;
    }

    @Override
    public Boolean loadInBackground() {
        for (String path : savedFilesForNote) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(NotesContract.Files.COLUMN_PATH, path);
            contentValues.put(NotesContract.Files.COLUMN_NOTE_ID, noteId);
            Log.i("Test", "Добавляем в бд файл по путю " + path);
            Uri insertedFileUri = getContext().getContentResolver().insert(NotesContract.Files.URI, contentValues);
            if (insertedFileUri == null) {
                Log.wtf("Test", "Не смогли добавить " + path);
                savedFilesForNote = null;
                return false;
            }
            Log.i("Test", "Добавили " + path + " Uri: " + insertedFileUri);
            savedFilesForNote = null;
        }
        return true;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
