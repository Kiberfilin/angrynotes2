package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Created by User on 11.02.2018.
 */

public class ImageDeletingLoader extends AsyncTaskLoader<Boolean> {
    private Uri uri;

    public ImageDeletingLoader(Context context, Uri uri) {
        super(context);
        this.uri = uri;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public Boolean loadInBackground() {
            return getContext().getContentResolver().delete(uri,
                    null,
                    null) != 0;
    }
}
