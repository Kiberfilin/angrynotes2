package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Created by User on 09.02.2018.
 */

public class ImageAddingLoader extends AsyncTaskLoader<Boolean> {
    private ArrayList<String> savedImagesForNote;
    private Long noteId;
    public ImageAddingLoader(Context context, ArrayList<String> savedImagesForNote, Long noteId) {
        super(context);
        this.savedImagesForNote = savedImagesForNote;
        this.noteId = noteId;
    }

    @Override
    public Boolean loadInBackground() {
        for (String path : savedImagesForNote) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(NotesContract.Images.COLUMN_PATH, path);
            contentValues.put(NotesContract.Images.COLUMN_NOTE_ID, noteId);
            Log.i("Test", "Добавляем в бд изображение по путю " + path);
            Uri insertedImageUri = getContext().getContentResolver().insert(NotesContract.Images.URI, contentValues);
            if (insertedImageUri == null) {
                Log.wtf("Test", "Не смогли добавить " + path);
                savedImagesForNote = null;
                return false;
            }
            Log.i("Test", "Добавили " + path + " Uri: " + insertedImageUri);
            savedImagesForNote = null;
        }
        return true;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
