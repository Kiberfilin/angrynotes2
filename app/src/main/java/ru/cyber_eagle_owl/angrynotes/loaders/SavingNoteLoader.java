package ru.cyber_eagle_owl.angrynotes.loaders;

import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import ru.cyber_eagle_owl.angrynotes.db.NotesContract;

/**
 * Created by User on 31.01.2018.
 */

public class SavingNoteLoader extends AsyncTaskLoader<Boolean> {
    public static Uri createdNoteUri;
    private ContentValues contentValues;

    public SavingNoteLoader(Context context, ContentValues contentValues) {
        super(context);
        this.contentValues = contentValues;
    }

    @Override
    public Boolean loadInBackground() {
        Log.i("Test", "Старт loadInBackground");
        createdNoteUri = getContext().getContentResolver().insert(NotesContract.Notes.URI, contentValues);
        Log.i("Test", "createdNoteUri = " + createdNoteUri);
        return createdNoteUri != null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
